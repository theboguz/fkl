SOURCES = $(shell find . -type f -name '*.cpp')
HEADERS = $(shell find . -type f -name '*.h')
OBJS = $(SOURCES:.cpp=.o)

.PHONY: all clean run

all: $(OBJS) $(HEADERS)
	g++ $^ -o fkl

clean:
	rm -rf $(OBJS)
	rm -f fkl

run: all
	./fkl

%.o: %.cpp
	g++ -c $< -o $@