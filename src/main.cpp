#include "include/lexer.h"
#include "include/parser.h"

#include <iostream>


#include <fstream>
#include <sstream>


std::string readFileToString(std::string filename) {
    std::ifstream file(filename);
    std::stringstream fileStream;
    fileStream << file.rdbuf();

    return fileStream.str();
}


int main()
{
    Lexer lexer(readFileToString("fklexample.fkl"));
    lexer.collect();
    TokenManager tokens = lexer.tokenList;



    Parser parser(tokens);

    Expr* AST = parser.parse();
    PrintVisitor* visitor = new PrintVisitor;

    AST->accept(visitor);




    // Token tok1(TOK_INT, "5", 1);
    // Token tokOp(TOK_PLUS, "+", 1);
    // Token tok2(TOK_INT, "3", 1);
    // Token tok3(TOK_INT, "7", 1);
    // Token tok4(TOK_INT, "9", 1);

    // Token minus(TOK_MINUS, "-", 1);

    // LiteralExpr literal1(&tok1);
    // LiteralExpr literal2(&tok2);
    // LiteralExpr literal3(&tok3);
    // LiteralExpr literal4(&tok4);
 
    
    // BinaryExpr bin(&literal1, &tokOp, &literal2);
    // BinaryExpr bin2(&literal3, &tokOp, &literal4);
    // BinaryExpr binRoot(&bin, &tokOp, &bin2);

    // PrintVisitor* visitor = new PrintVisitor;

    // UnaryExpr un(&minus, &bin);


    // un.accept(visitor);
}