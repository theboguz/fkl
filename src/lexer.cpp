#include "include/lexer.h"

void Lexer::advance()
{
    if (currentChar)
    {
        currentIndex++;
        currentChar = source[currentIndex];
    }
}

void Lexer::collect()
{
    while (currentChar)
    {
        if (isalpha(currentChar))
            collectID();
        else if(isdigit(currentChar))
            collectNumber();
        

        switch (currentChar)
        {
            case '\'': collectString(); break;

            // Spaces, newlines 
            case ' ': 
            case '\t': advance(); break;
            case '\n': advance(); currentLine++; break;

            case ';': tokenList.push_back(Token(TOK_SEMICOLON, ";", currentLine)); advance(); break;

            // Delimiters
            case '{': tokenList.push_back(Token(TOK_LBRACE, "{", currentLine)); advance(); break;
            case '}': tokenList.push_back(Token(TOK_RBRACE, "}", currentLine)); advance(); break;
            case '(': tokenList.push_back(Token(TOK_LPAREN, "(", currentLine)); advance(); break;
            case ')': tokenList.push_back(Token(TOK_RPAREN, ")", currentLine)); advance(); break;
            case '[': tokenList.push_back(Token(TOK_LBRACKET, "[", currentLine)); advance(); break;
            case ']': tokenList.push_back(Token(TOK_RBRACKET, "]", currentLine)); advance(); break;
            
            case '+': tokenList.push_back(Token(TOK_PLUS, "+", currentLine)); advance(); break;
            case '-': tokenList.push_back(Token(TOK_MINUS, "-", currentLine)); advance(); break;
            case '*': tokenList.push_back(Token(TOK_STAR, "*", currentLine)); advance(); break;
            case '/': tokenList.push_back(Token(TOK_SLASH, "/", currentLine)); advance(); break;

            case '=': {
                if (source[currentIndex +1] == '=')
                    tokenList.push_back(Token(TOK_EQUALSEQUALS, "==", currentLine)); advance(); break;
            }


            // Assign

            default: advance(); break;
        }
        
    }

    tokenList.push_back(Token(TOK_EOF, "EOF", currentLine));
}
void Lexer::collectID()
{
    std::string buffer;

    while (isalpha(currentChar))
    {
        buffer += currentChar;
        advance();
    }

    tokenList.push_back(Token(TOK_ID, buffer, currentLine));
}

void Lexer::collectNumber()
{
    std::string buffer;

    while (isdigit(currentChar))
    {
        buffer += currentChar;
        advance();
    }

    tokenList.push_back(Token(TOK_INT, buffer, currentLine));
}

void Lexer::collectString()
{
    std::string buffer;

    advance();

    while (currentChar != '\'')
    {
        buffer += currentChar;
        advance();
    }

    advance();

    tokenList.push_back(Token(TOK_STRING, buffer, currentLine));
}

void Lexer::findKeywords()
{

}