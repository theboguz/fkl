#include "include/parser.h"


bool Parser::check(TokenType type)
{
    if (tokens.isAtEnd()) {return false;}
    return tokens.current()->type == type;
}

bool Parser::match(TokenType type)
{
    if (check(type))
    {
        tokens.advance();
        return true;
    }

    return false;
}

Expr* Parser::expression()
{
    return equality();
}

Expr* Parser::factor()
{
    Expr* expr = unary();

    while (match(TOK_STAR) || match(TOK_SLASH))
    {
        Token* op = tokens.previous();
        Expr* right = unary();
        expr = new BinaryExpr(expr, op, right);
    }



    return expr;
}
Expr* Parser::term()
{
    Expr* expr = factor();

    while(match(TOK_MINUS) || match(TOK_PLUS))
    {
        Token* op = tokens.previous();
        Expr* right = factor();
        expr = new BinaryExpr(expr, op, right);
    }

    return expr;
}

Expr* Parser::unary()
{
    if (match(TOK_NEGATE) || match(TOK_MINUS))
    {
        Token* op = tokens.previous();
        Expr* right = unary();
        return new UnaryExpr(op, right);
    }


    
    return primary();
}

Expr* Parser::parse() {
    return expression();
}

Expr* Parser::primary()
{   

    if (match(TOK_INT) || match(TOK_STRING)) {return new LiteralExpr(tokens.previous());}

}


Expr* Parser::equality()
{
    Expr* expr = comparision();

    while (match(TOK_NEGATEEQUALS) || match(TOK_EQUALSEQUALS))
    {
        Token* op = tokens.previous();
        Expr* right = comparision();
        expr = new BinaryExpr(expr, op, right);
    }



    return expr;
}

Expr* Parser::comparision()
{
    Expr* expr = term();

    while (match(TOK_GREATER) || match(TOK_GREATER_EQUALS) || match(TOK_LESSER) || match(TOK_LESSEREQUALS))
    {
        Token* op = tokens.previous();
        Expr* right = term();
        expr = new BinaryExpr(expr, op, right);
    }


    return expr;
}