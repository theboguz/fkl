#ifndef TOKENMANAGER_H
#define TOKENMANAGER_H


#include <vector>
#include "token.h"

class TokenManager {
    private:
        std::vector<Token> tokens;
    public:
        unsigned int index;
        Token* currentToken;


        Token* previous() {return &tokens[index-1];}
        Token* current() {return &tokens[index];}
        Token* next() {return &tokens[index+1];}

        void advance() {
            index++;
            currentToken = &tokens[index];
        }

        Token* eat(TokenType type) 
        {
            if (currentToken->type == type)
            {
                advance();
                return currentToken;
            } else {
                std::cout << "Error, unexpected token on line: " << currentToken->line << std::endl;
                exit(1);
            }

        }

        bool isFirstToken() {return index == 0;}
        bool isLastToken() {return index == tokens.size();}

        bool isAtEnd() {return current()->type == TOK_EOF;}


        TokenManager(std::vector<Token> tokens) : tokens(tokens), index(0), currentToken(&tokens[0]) {}

        void out()
        {
            while (!isLastToken())
            {
                current()->out();
                advance();
            }
        }
};

#endif