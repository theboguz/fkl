#ifndef PARSER_H
#define PARSER_H

#include "tree.h"
#include "tokenmanager.h"

class Parser {
    public:
        TokenManager tokens;
        unsigned int index;

        Parser(TokenManager& tokens) : tokens(tokens), index(0) {}


        bool check(TokenType type);
        bool match(TokenType type);

        Expr* parse();

        Expr* expression();
        Expr* factor();
        Expr* term();
        Expr* unary();
        Expr* primary();

        //

        Expr* equality();
        Expr* comparision();
        

};

#endif