#ifndef TOKEN_H
#define TOKEN_H


#define LOG(x) std::cout << x;

enum TokenType {
    // Delimiters
    TOK_LPAREN, TOK_RPAREN,
    TOK_LBRACE, TOK_RBRACE,
    TOK_LBRACKET, TOK_RBRACKET,
    
    // Pontuation
    TOK_DOT, TOK_COMMA, TOK_SEMICOLON, TOK_COLON, 

    // Math Operators
    TOK_PLUS, TOK_MINUS, TOK_STAR, TOK_SLASH,
    TOK_PERCENT, TOK_STARSTAR,

    // Assignment Operators
    TOK_EQUALS, TOK_PLUSEQUALS, TOK_MINUSEQUALS, TOK_STAREQUALS, TOK_SLASHEQUALS, TOK_OREQUALS, TOK_ANDEQUALS,

    // Comparision Operators
    TOK_EQUALSEQUALS, TOK_GREATER, TOK_LESSER, TOK_GREATER_EQUALS, TOK_LESSEREQUALS, TOK_NEGATEEQUALS,

    // Keywords
    TOK_IMPL, TOK_OFFSET, TOK_FALSE, TOK_TRUE,

    TOK_NEGATE,
    // Types


    TOK_STRING, TOK_ID, TOK_INT, TOK_NOOP,

    TOK_EOF,
};

#include <string>
#include <iostream>



class Token {
    public:
        TokenType type;
        std::string value;
        unsigned int line;

        Token(TokenType type, std::string value, unsigned int line) : type(type), value(value), line(line) {}

        void operator()() {std::cout << "(" + std::to_string(type) + " : " + value + ")" << std::endl;}
        void out() {std::cout << "(" + std::to_string(type) + " : " + value + ")" << std::endl;}
        void outValue() {std::cout << value;}
};

#endif