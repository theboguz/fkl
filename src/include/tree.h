#ifndef TREE_H
#define TREE_H

#include "token.h"

class UnaryExpr;
class BinaryExpr;
class LiteralExpr;

class Visitor {
    public:
        virtual void visit(BinaryExpr* el) {}
        virtual void visit(LiteralExpr* el) {}
        virtual void visit(UnaryExpr* el) {}
};

class Expr {
    public:
        virtual void accept(Visitor* visitor) {}
};

class UnaryExpr: public Expr {
    public:
        Token* op;
        Expr* right;

        UnaryExpr(Token* op, Expr* right) : op(op), right(right) {}

        void accept(Visitor* v) {
            v->visit(this);
        }
};

class BinaryExpr: public Expr {
    public:
        Expr* leftSide;
        Token* op;
        Expr* rightSide;

        BinaryExpr(Expr* left, Token* op, Expr* right) : leftSide(left), rightSide(right), op(op) {}

        void accept(Visitor* v) {
            v->visit(this);
        }
};

class LiteralExpr: public Expr {
    public:
        Token* literal; 

        LiteralExpr(Token* literal) : literal(literal) {}

        void accept(Visitor* v)   {
            v->visit(this);
        }
};

class PrintVisitor: public Visitor {
    public:
        void visit(BinaryExpr* el) override {
            LOG("(")
            el->leftSide->accept(this);
            el->op->outValue();
            el->rightSide->accept(this);
            LOG(")")
        }
        void visit(LiteralExpr* el) override {
           el->literal->outValue();
        }

        void visit(UnaryExpr* el) override {
            LOG("(")
            el->op->outValue();
            el->right->accept(this);
            LOG(")")
        }
};


#endif