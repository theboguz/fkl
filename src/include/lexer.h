#ifndef LEXER_H
#define LEXER_H

#include "token.h"
#include <vector>
#include <string>

class Lexer {
    public:
        char currentChar;
        unsigned int currentIndex;
        unsigned int currentLine;
        std::string source;

        std::vector<Token> tokenList;

        Lexer(std::string source) : source(source), currentIndex(0), currentChar(source[0]) {}


        void collect();
        void collectID();
        void collectNumber();
        void collectString();
        void findKeywords();
        
        void advance();
};

#endif